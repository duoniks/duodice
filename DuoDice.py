#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tkinter import *
import tkinter.ttk as ttk

import requests
import random
import time
import os.path
import json

KEY = '12903382f7fb4cbd9d305bab705db969'
FILE = 'user.json'

class Bot(object):

    def __init__(self):
        self.Currencies = 'btc'
        self.start = False
        self.stop = False
        self.Lo_bet = True #Меньше или больше
        self.base_payIn = 1
        self.set_win_chance()

    def dat(self, name):
        d = {}
            
        if name == 'CreateAccount': #Создание акаунта
            d.update(a=name)
            d.update(Key=KEY)

        if name == 'BeginSession':
            d.update(a=name)
            d.update(Key=KEY)
            d.update(AccountCookie=self.AccountCookie)

        if name == 'Login': #Зайти по имеющимся логину и паролю
            d.update(a=name)
            d.update(Key=KEY)
            d.update(Username=self.username)
            d.update(Password=self.password)

        if name == 'GetBalance': #Обновление баланса
            d.update(a=name)
            d.update(s=self.SessionCookie)
            d.update(Currency=self.Currencies)

        if name == 'PlaceBet':
            d.update(a=name)
            d.update(s=self.SessionCookie)
            d.update(PayIn=self.payIn)
            d.update(Low=self.bet()[0])
            d.update(High=self.bet()[1])
            d.update(ClientSeed=self.ClientSeed)
            d.update(Currency=self.Currencies)

        if name == 'GetCurrencies':
            d.update(a=name)

        return d

    def seq(self, name):
        d = self.dat(name)
        json = self._requests(d)
        self.update(json)

    def update(self, json):
        for key, val in json.items():
            setattr(self, key, val)
    
    def _requests(self, data):
        rs = requests
        while True:
            try:
                r = rs.post('https://www.999dice.com/api/web.aspx', data=data)
                return r.json()
            except requests.exceptions.RequestException:
                time.sleep(30)
                continue
            except Exception:
                time.sleep(30)
                continue


    def get_atribute(self, name):
        '''
        Получить значение любого атрибута
        Если его не существует тогда создаст со значением = 0
        '''
        while True:
            try:
                return getattr(self, name)
            except AttributeError:
                setattr(self, name, 0)
                continue

    def set_win_chance(self, num=47.5):
        '''
        Устанавливает шанс выиграша
        '''
        self.win_chance = num
        self.b = [[0, int(999999 * (self.win_chance / 100))],
                [int(1000000 - 999999 * (self.win_chance / 100)), 999999]]

    def bet(self):
        if self.Lo_bet:
            return self.b[0]
        else:
            return self.b[1]

    def change_Lo_bet(self):
        '''
        Вызов этой функции изменяет руку ставок
        '''
        self.Lo_bet = not self.Lo_bet

    def _login(self, name, password):
        self.username = name
        self.password = password
        self.seq('Login')

    def get_currencies(self):
        self.seq('GetCurrencies')
        return self.Currencies

    def set_currencies(self, w=0):
        '''Выбор кошелька
        BTC = 0
        DOGE = 1
        LTC = 2
        ETH = 3
        '''
        self.seq('GetCurrencies')
        self.Currencies = self.Currencies[w]

    def get_balance(self):
        '''
        Получить баланс.
        Возвращает строку
        '''
        self.seq('GetBalance')
        return '{0:.8f}'.format(self.get_atribute('Balance')/100000000)

    def set_base_payIn(self, num=None):
        '''
        Установка базовой ставки
        '''
        if num == None:
            self.payIn = self.base_payIn
        else:
            self.base_payIn = num
            self.payIn = self.base_payIn

    def change_payIn(self):
        '''
        Увеличение ставки
        '''
        self.payIn += self.payIn

    win = True
    text = ''

    stat = {'win': 0, 'lose': 0}

    def init_stat(self):
        self.p = self.base_payIn
        b = []
        for x in range(0, 16):
            b.append(0)
        self.stat.update(bet=b)


    str_stat = ''

    def Stat(self, num=None):
        self.p = self.base_payIn
        self.str_stat = 'win: {}, lose: {},'.format(self.stat['win'], self.stat['lose'])
        if num != None:
            self.stat['bet'][num] += 1
        for i in range(16):
            self.str_stat += ' <{}: {}>'.format(self.p, self.stat['bet'][i])
            self.p = self.p + self.p


    num_bet = 0
    def checking_bet(self):
        '''
        Проверка ставки
        '''
        if self.Lo_bet:
            if self.Secret < self.bet()[1]: #Малая рука
                self.win = True
                self.text = '{:06} {} {} {:.8f}'.format(self.Secret, 'Win ', 'Bet: ', self.payIn/100000000)
                self.stat['win'] += 1
                self.Stat()
                self.num_bet = 0
            else:
                self.win = False
                self.text = '{:06} {} {} {:.8f}'.format(self.Secret, 'Lose', 'Bet: ', self.payIn/100000000)
                self.stat['lose'] += 1
                self.Stat(self.num_bet)
                self.num_bet += 1
        else:
            if self.Secret > self.bet()[0]: #Большая рука
                self.win = True
                self.text = '{:06} {} {} {:.8f}'.format(self.Secret, 'Win ', 'Bet: ', self.payIn/100000000)
                self.stat['win'] += 1
                self.Stat()
                self.num_bet = 0
            else:
                self.win = False
                self.text = '{:06} {} {} {:.8f}'.format(self.Secret, 'Lose', 'Bet: ', self.payIn/100000000)
                self.stat['lose'] += 1
                self.Stat(self.num_bet)
                self.num_bet += 1



def main():
    user_dat = []
    bot = Bot()

    root = Tk()
    root.iconbitmap('logo.ico')
    root.title('DuoDice')
    root.geometry("800x500")

    frame_top = Frame(root)
    frame_top.pack(fill=BOTH)

    name_entry_login = Label(frame_top, text='Login:       ')
    name_entry_login.pack(side=LEFT)


    login_name = StringVar()
    entry_login = Entry(frame_top, textvariable=login_name)
    entry_login.pack(side=LEFT)

    frame_top_second = Frame(root)
    frame_top_second.pack(fill=BOTH)

    frame_top_statistic = Frame(root)
    frame_top_statistic.pack(fill=BOTH)

    Statistic = StringVar()
    Statistic.set(bot.str_stat)
    stat = Label(frame_top_statistic, textvariable=Statistic)
    stat.pack(side=LEFT)

    name_entry_password = Label(frame_top_second, text='Password:')
    name_entry_password.pack(side=LEFT)

    login_password = StringVar()
    entry_password = Entry(frame_top_second, textvariable=login_password, show='*')
    entry_password.pack(side=LEFT)

    def select_walet(evt):
        bot.Currencies = walet.get()
        b.set(bot.get_balance())

    wal = ['btc','doge','ltc','eth']
    walet = ttk.Combobox(frame_top,values=wal, width=5)
    walet.set(wal[0])
    walet.bind('<<ComboboxSelected>>', select_walet)
    walet.pack(side=LEFT)

    b = StringVar()
    b.set('0.00000000')
    balance = Label(frame_top, textvariable=b)
    balance.pack(side=LEFT)

    name_entry_bet = Label(frame_top, text='Базовая ставка:')
    name_entry_bet.pack(side=LEFT)

    login_bet = StringVar()
    entry_bet = Entry(frame_top, width=10, textvariable=login_bet)
    entry_bet.pack(side=LEFT)

    name_entry_chance = Label(frame_top, text='Шанс выиграша:')
    name_entry_chance.pack(side=LEFT)

    login_chance = StringVar()
    entry_chance = Entry(frame_top, width=10, textvariable=login_chance)
    entry_chance.pack(side=LEFT)

    name_entry_num = Label(frame_top, text='Любимое число:')
    name_entry_num.pack(side=LEFT)

    login_num = StringVar()
    entry_num = Entry(frame_top, width=10, textvariable=login_num)
    entry_num.pack(side=LEFT)

    def my_login():
        if not os.path.exists(FILE) or user_dat['login'] != entry_login.get() or user_dat['num'] != entry_num.get() or user_dat['bet'] != entry_bet.get() or user_dat['chance'] != entry_chance.get():
            dat = {'login': entry_login.get(),
                    'password': entry_password.get(),
                    'bet': entry_bet.get(),
                    'chance': entry_chance.get(),
                    'num': entry_num.get()}
            j = json.dumps(dat)
            file = open(FILE, 'w')
            json.dump(j, file, skipkeys=True, sort_keys=True)

        bot._login(entry_login.get(), entry_password.get())
        bot.set_base_payIn(int(entry_bet.get()))
        bot.init_stat()
        bot.set_win_chance(float(entry_chance.get()))
        bot.ClientSeed = int(entry_num.get())
        bot.payIn = int(entry_bet.get())
        b.set(bot.get_balance())

    login = Button(frame_top_second, text='Login', command=my_login)
    login.pack(side=LEFT)

    frame_loop = Frame(root)
    frame_loop.pack(fill=BOTH)

    scrol = Scrollbar(frame_loop)
    scrol.pack(side=RIGHT, fill=BOTH)

    #mylist = Listbox(frame_loop, bg='black', height=150,width=300)
    #mylist = Listbox(frame_loop, bg='black', height=150,width=300, font='Arial 14')
    mylist = Listbox(frame_loop, height=150,width=300)
    mylist.pack()

    def LO_loop():
        bot.Lo_bet = True
        bot.set_base_payIn(int(entry_bet.get()))
        bot.set_win_chance(float(entry_chance.get()))
        bot.ClientSeed = int(entry_num.get())
        bot.payIn = int(entry_bet.get())
        loop()

    def HI_loop():
        bot.Lo_bet = False
        bot.set_base_payIn(int(entry_bet.get()))
        bot.set_win_chance(float(entry_chance.get()))
        bot.ClientSeed = int(entry_num.get())
        bot.payIn = int(entry_bet.get())
        loop()

    def loop():
        secret = 0
        error_none = True
        bot.seq('PlaceBet')
        bot.checking_bet()
        b.set(bot.get_balance())
        Statistic.set(bot.str_stat)
        mylist.insert(0, bot.text)

        if secret == bot.Secret or bot.get_atribute('error') != 0:
            mylist.insert(0, 'Произошла ошибка')
            mylist.itemconfig(0, {'bg': 'red'})
            error_none = False
            bot.stop = True

        if error_none:
            if bot.win:
                bot.payIn = bot.base_payIn
                mylist.itemconfig(0, {'fg': 'green'})
                bo = True
            else:
                bot.payIn += bot.payIn
                mylist.itemconfig(0, {'fg': 'red'})
                bo = False

        if bot.payIn > bot.Balance:
            mylist.insert(0, 'На счету меньше чем ставка')
            mylist.itemconfig(0, {'bg': 'red'})
            bot.stop = True

        if bot.start and bo:
            bot.start = False
            pass
        elif bot.stop:
            bot.stop = False
            pass
        else:
            mylist.after(2000, loop)


    scrol['command'] = mylist.yview
    mylist['yscrollcommand'] = scrol.set

    LO_loop_but = Button(frame_top_second, text='Меньше',command=LO_loop)
    LO_loop_but.pack(side=LEFT)

    HI_loop_but = Button(frame_top_second, text='Больше',command=HI_loop)
    HI_loop_but.pack(side=LEFT)

    def stop():
        bot.start = True

    def stop_roll():
        bot.stop = True

    def resume():
        loop()

    stop_rollback = Button(frame_top_second, text='Остановить при выиграше',command=stop)
    stop_rollback.pack(side=LEFT)

    stop = Button(frame_top_second, text='Остановить',command=stop)
    stop.pack(side=LEFT)

    resume = Button(frame_top_second, text='Продолжить',command=resume)
    resume.pack(side=LEFT)
    
    if os.path.exists(FILE):
        file = open(FILE, 'r')
        j = json.load(file)
        user_dat = json.loads(j)
        login_name.set(json.loads(j)['login'])
        login_password.set(json.loads(j)['password'])
        login_bet.set(json.loads(j)['bet'])
        login_chance.set(json.loads(j)['chance'])
        login_num.set(json.loads(j)['num'])

    root.mainloop()


if __name__ == '__main__':
    main()