# -*- coding: utf-8 -*-
import os
from cx_Freeze import setup, Executable

os.environ['TCL_LIBRARY'] = 'C:/Python/tcl/tcl8.6'
os.environ['TK_LIBRARY'] = 'C:/Python/tcl/tk8.6'

buildOptions = dict(
    packages = ['requests','urllib3', 'queue','idna'],
    excludes = [],
    include_files=['C:/Python/DLLs/tcl86t.dll', 'C:/Python/DLLs/tk86t.dll']
)

import sys
base = 'Win32GUI' if sys.platform=='win32' else None
#base = 'Console' if sys.platform=='win32' else None

executables = [Executable(
    'DuoDice.py',
    base = base,
    icon="logo.ico")]

setup(
    name = 'DuoDice',
    version = '0.1',
    description = "Dice",
    author="Duoniks",
    options = dict(build_exe = buildOptions),
                    executables = executables)