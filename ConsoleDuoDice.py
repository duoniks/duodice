#!/usr/bin/env python
# -*- coding: utf-8 -*-
import requests
import random
from ctypes import *
import time


windll.Kernel32.GetStdHandle.restype = c_ulong
h = windll.Kernel32.GetStdHandle(c_ulong(0xfffffff5))
key = '12903382f7fb4cbd9d305bab705db969'

class Bot(object):
    username = None
    password = None

    SessionCookie = None
    AccountId = None
    ClientSeed = None
    Balance = 0
    StartingBalance = 1
    Currency = 'btc'
    Next = ''
    Secret = 0
    BetId = 0
    LoginInvalid = None # Не правильный логин или пароль
    InsufficientFunds = None # Недостаточно средств
    NoPossibleProfit = None # Не возможно забрать ставку
    MaxPayoutExceeded = None # Превышение максимальной выплаты казино

    error = None

    base_payIn = 1
    payIn = 1

    win_lose = ''
    Lo_bet = True
    bet = None

    def __init__(self, username=None, password=None, win_chance=49.95, LO=True):
        self.Lo_bet = LO
        if username != None and password != None:
            self.username = username
            self.password = password
            self.seq('Login')
            while self.AccountId == None:
                if self.error != None:
                    print('Бот заблокирован на 2 мин.')
                    time.sleep(130)
                    self.error = None
                else:
                    print('Сайт не доступен.')
                    time.sleep(30)
                
                self.seq('Login')
        else:
            #self.seq('CreateAccount')
            pass

        self.b = [[0, int(999999 * (win_chance / 100))], [int(1000000 - 999999 * (win_chance / 100)), 999999]]
        self.set_LO_HI()

    def set_LO_HI(self):
        if self.Lo_bet:
            self.bet = self.b[0]
        else:
            self.bet = self.b[1]

    def set_base_payIn(self, num=1):
        self.base_payIn = num
        self.payIn = self.base_payIn
        

    def set_clent_seed(self, num=0):
        self.ClientSeed = num
        
    def setCurrency(self, num=0):
        cur = ['btc', 'doge', 'ltc', 'eth']
        self.Currency = cur[num]

    def seq(self, name):
        d = self.dat(name)
        json = self.req_proc(d)
        self.update(json)

    def dat(self, name):
        d = {}
            
        if name == 'CreateAccount': #Создание акаунта
            d.update(a=name)
            d.update(Key=key)

        if name == 'Login': #Зайти по имеющимся логину и паролю
            d.update(a=name)
            d.update(Key=key)
            d.update(Username=self.username)
            d.update(Password=self.password)

        if name == 'GetBalance': #Обновление баланса
            d.update(a=name)
            d.update(s=self.SessionCookie)
            d.update(Currency=self.Currency)

        if name == 'PlaceBet':
            d.update(a=name)
            d.update(s=self.SessionCookie)
            d.update(PayIn=self.payIn)
            d.update(Low=self.bet[0])
            d.update(High=self.bet[1])
            d.update(ClientSeed=self.ClientSeed)
            d.update(Currency=self.Currency)

        return d

    def req_proc(self, data):
        #rs = requests.Session()
        rs = requests
        while True:
            try:
                r = rs.post('https://www.999dice.com/api/web.aspx', data=data)
                return r.json()
            except requests.exceptions.HTTPError:
                windll.Kernel32.SetConsoleTextAttribute(h, 15)
                print('Ошибка HTTPError')
                time.sleep(60)
            except Exception as e:
                windll.Kernel32.SetConsoleTextAttribute(h, 15)
                print('{} {}'.format('Непредвиденная ошибка', e))
                time.sleep(60)
        
        

    def update(self, json):
        for key, val in json.items():
            setattr(self, key, val)

    def getBalance(self):
        self.seq('GetBalance')
        return '{0:.8f}'.format(self.Balance/100000000)

    def valid_payIn(self):
        mas = ''
        if self.error != None:
            windll.Kernel32.SetConsoleTextAttribute(h, 15)
            mas = 'Бот заблокирован на 2 мин. '
            time.sleep(60)
            self.error = None

        if self.NoPossibleProfit != None:
            windll.Kernel32.SetConsoleTextAttribute(h, 15)
            mas = 'Не возможно забрать ставку. '

        if self.MaxPayoutExceeded != None:
            windll.Kernel32.SetConsoleTextAttribute(h, 15)
            mas = 'Превышение максимальной выплаты казино. '

        mas += self.Next
        print(mas)

    def change_bet(self):
        if self.Lo_bet:
            if self.Secret < self.bet[1]: #bet[0] = 0 bet[1] = 475999
                self.payIn = self.base_payIn
                #self.Lo_bet = not self.Lo_bet
            else:
                self.payIn += self.payIn
        else:
            if self.Secret > self.bet[0]: #bet[0] = 525000 bet[1] = 999999
                self.payIn = self.base_payIn
                #self.Lo_bet = not self.Lo_bet
            else:
                self.payIn += self.payIn

    def change_win_lose(self):
        if self.Lo_bet:
            if self.Secret < self.bet[1]: #bet[0] = 0 bet[1] = 475999
                windll.Kernel32.SetConsoleTextAttribute(h, 10)
                self.win_lose = 'Win '
            else:
                windll.Kernel32.SetConsoleTextAttribute(h, 12)
                self.win_lose = 'Lose'
        else:
            if self.Secret > self.bet[0]: #bet[0] = 525000 bet[1] = 999999
                windll.Kernel32.SetConsoleTextAttribute(h, 10)
                self.win_lose = 'Win '
            else:
                windll.Kernel32.SetConsoleTextAttribute(h, 12)
                self.win_lose = 'Lose'

    def start_bot(self):
        temp_bId = 0
        bId = [0]
        max_bet = [0]
        while self.Balance != 0:
            self.seq('PlaceBet')

            if self.InsufficientFunds != None:
                windll.Kernel32.SetConsoleTextAttribute(h, 15)
                i =  int(input('Недостаточно средств.\nНажмите 0 для выхода.\nНажмите 1 минимальная ставка.\nНажмите 2 максимальная ставка\n'))
                if i == 1:
                    self.InsufficientFunds = None
                    self.payIn = self.base_payIn
                    continue

                if i == 2:
                    self.seq('GetBalance')
                    self.InsufficientFunds = None
                    self.payIn = self.Balance
                    continue

                if i == 0:
                    break
                    
            if bId.pop() == self.BetId:
                windll.Kernel32.SetConsoleTextAttribute(h, 15)
                print('Что-то не так ')
                self.valid_payIn()
                time.sleep(60)
                self.seq('GetBalance')
                bId.append(self.BetId)
                continue
            bId.append(self.BetId)

            self.change_win_lose()

            print('{0:06} {1} Bet: {2:.8f} Balance: {3}'.format(self.Secret, self.win_lose, self.payIn/100000000, self.getBalance()))
            temp_bId = self.BetId
            time.sleep(2)

            self.change_bet()
            self.set_LO_HI()
            if self.payIn > self.Balance:
                windll.Kernel32.SetConsoleTextAttribute(h, 15)
                i =  int(input('Недостаточно средств.\nНажмите 0 для выхода.\nНажмите 1 минимальная ставка.\nНажмите 2 максимальная ставка\n'))
                if i == 1:
                    self.InsufficientFunds = None
                    self.payIn = self.base_payIn
                    continue

                if i == 2:
                    self.seq('GetBalance')
                    self.InsufficientFunds = None
                    self.payIn = self.Balance
                    continue

                if i == 0:
                    break


def main():
    username = input('Login \n')
    password = input('Password \n')
    w_chance = float(input('Win chancs \n'))
    lo = input('LO y/n \n')
    if lo == 'y' or lo == 'Y':
        l = True
    else:
        l = False
    #Логин, пароль, шанс выиграша, LO=False для игры на больших числах
    bot = Bot(username, password, win_chance=w_chance, LO=l)
    bot.setCurrency(num=0) #Выбор валюты 0 = btc, 1 = doge, 2 = ltc, 3 = eth
    bot.set_clent_seed('5654') #Выбор клентского сида 8-ми значное число
    print(bot.Currency, bot.ClientSeed)
    bot.set_base_payIn(1) #Выбор минимальной ставки в сатошах
    bot.start_bot()

if __name__ == '__main__':
    main()
    